package com.imtiaz.mobilepaytest.ui.base

import android.annotation.TargetApi
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.webkit.URLUtil
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import com.imtiaz.mobilepaytest.BR
import com.imtiaz.mobilepaytest.R

abstract class BaseActivity<BINDING : ViewDataBinding>(@LayoutRes val layoutId: Int) :
    AppCompatActivity() {

    protected lateinit var binding: BINDING

    private lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, layoutId)
        binding.lifecycleOwner = this
        binding.executePendingBindings()
        setContentView(binding.root)
    }

    @TargetApi(Build.VERSION_CODES.M)
    open fun hasPermission(permission: String?): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission!!) == PackageManager.PERMISSION_GRANTED
    }

    open fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showKeyboard() {
        val imm = (getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    /**
     * Navigates to given destination with [defaultDestination] and [deepLink]
     * [deepLink] will get priority over [defaultDestination]
     * @param defaultDestination is the direction of the destination
     * @param deepLink is the deep link
     * @param navOptions is the [NavOptions] that is expected to run while navigation
     */
    fun navigateTo(
        defaultDestination: NavDirections? = null,
        deepLink: String? = null,
        navOptions: NavOptions? = null
    ) {
        if (defaultDestination == null && deepLink == null) {
            Log.e("navigateTo:","No defaultDestinationId or deepLink provided")
            return
        }
        try {
            val controller = findNavController(R.id.nav_host_fragment)
            if (deepLink != null) {
                processDeepLink(this, controller, Uri.parse(deepLink), navOptions)
                return
            }
            defaultDestination?.let { controller.navigate(it, navOptions) }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * This method will help to find app deeplink, if found it will safely navigate there
     * or other wise will try validated the deeplink is valid URL or not,
     * if valid then safely open the URL
     * @param context is context of the current activity
     * @param controller is app navigation controller from nav host.
     * @param deepLink will contain app deepLink or web link.
     */
    private fun processDeepLink(
        context: Context,
        controller: NavController,
        deepLink: Uri,
        navOptions: NavOptions? = null
    ) {
        if (controller.graph.hasDeepLink(deepLink)) {
            controller.navigate(deepLink, navOptions)
        } else if (URLUtil.isValidUrl(deepLink.toString())) {
            try {
                val intent = Intent(Intent.ACTION_VIEW, deepLink)
                context.startActivity(intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun showLoader() {
        progressDialog = ProgressDialog(this@BaseActivity)
        progressDialog.setMessage("Please wait while loading...")
        progressDialog.show()
    }

    fun showLoader(loadinText: String) {
        progressDialog = ProgressDialog(this@BaseActivity)
        progressDialog.setMessage(loadinText)
        progressDialog.show()
    }

    fun dismissLoader() {
        try {
            if (this::progressDialog.isInitialized && progressDialog.isShowing)
                progressDialog.dismiss()

        } catch (exp: Exception) {
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    protected abstract fun initView()
}
