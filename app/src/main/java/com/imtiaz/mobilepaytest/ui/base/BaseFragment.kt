package com.imtiaz.mobilepaytest.ui.base

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import com.google.android.material.appbar.MaterialToolbar
import com.imtiaz.mobilepaytest.BR
import com.imtiaz.mobilepaytest.BuildConfig
import com.imtiaz.mobilepaytest.R

abstract class BaseFragment<BINDING : ViewDataBinding>(
    @LayoutRes private val layoutId: Int
) : Fragment() {
    private var _binding: BINDING? = null
    private var baseActivity: BaseActivity<BINDING>? = null

    protected val binding get() = _binding!!

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*>) {
            val activity: BaseActivity<BINDING> = context as BaseActivity<BINDING>
            baseActivity = activity
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        return _binding?.let {
            it.lifecycleOwner = viewLifecycleOwner
            it.executePendingBindings()
            printClassName()
            _binding?.root
        }
    }

    private fun printClassName() {
        if (BuildConfig.DEBUG) {
            try {
                Log.e("myClassName", javaClass.simpleName)
            } catch (exp: Exception) {
                Log.e("classNameExp", exp.toString())
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun changeStatusBarColor(colorId: Int = R.color.white, isSetTextColorWhite: Boolean = false) {
        val color = ContextCompat.getColor(requireContext(), colorId)
        baseActivity?.window?.apply {
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility =
                    if (isSetTextColorWhite) 0 else View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                statusBarColor = color
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                statusBarColor = color
        }
    }

    fun navigateTo(
        defaultDestination: NavDirections? = null,
        deepLink: String? = null,
        navOptions: NavOptions? = null
    ) = baseActivity?.navigateTo(defaultDestination, deepLink, navOptions)


    fun MaterialToolbar.setup(
        title: String = "",
        backgroundColor: Int = R.color.white,
        textColor: Int = R.color.black,
        menu: Int? = null,
        onBackNavigation:() -> Unit = { onBackPressed() }
    ) {
        setTitle(title)
        setBackgroundColor(ContextCompat.getColor(requireContext(), backgroundColor))
        setTitleTextColor(ContextCompat.getColor(requireContext(), textColor))
        setNavigationIconTint(ContextCompat.getColor(requireContext(), textColor))
        setNavigationOnClickListener { onBackNavigation() }
        menu?.let { inflateMenu(it) }
    }

    fun getBaseActivity(): BaseActivity<BINDING>? = baseActivity

    fun hideKeyboard() = baseActivity?.hideKeyboard()

    fun onBackPressed() = baseActivity?.onBackPressed()

    fun showLoader() = baseActivity?.showLoader()

    fun dismissLoader() = baseActivity?.dismissLoader()

    override fun onStop() {
        changeStatusBarColor()
        super.onStop()
    }

    protected abstract fun initView()
}
