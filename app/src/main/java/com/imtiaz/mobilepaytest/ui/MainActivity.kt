package com.imtiaz.mobilepaytest.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.imtiaz.mobilepaytest.R
import com.imtiaz.mobilepaytest.databinding.ActivityMainBinding
import com.imtiaz.mobilepaytest.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>(R.layout.activity_main) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    override fun initView() {

    }
}