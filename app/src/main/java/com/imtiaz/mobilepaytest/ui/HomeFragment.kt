package com.imtiaz.mobilepaytest.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.imtiaz.mobilepaytest.R
import com.imtiaz.mobilepaytest.databinding.FragmentHomeBinding
import com.imtiaz.mobilepaytest.ui.base.BaseFragment
import com.imtiaz.taskmanager.utils.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job


@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(R.layout.fragment_home), OnMapReadyCallback {

    private val viewModel: HomeViewModel by viewModels()
    private var mMap: GoogleMap? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initMap()
        observePlaces()
    }

    override fun initView() {
        binding.apply {
            layoutSearch.setOnClickListener {
                if(!binding.pbLoading.isVisible)
                    performSearch()
            }
            setSearchEditorListener(etSearchPlace)
        }
    }

    private fun setSearchEditorListener(editText: EditText) {
        editText.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                performSearch()
                return@OnEditorActionListener true
            }
            false
        })
    }

    private fun performSearch() {
        binding.apply {
            val searchText = etSearchPlace.text.toString()
            if (searchText.length > 1) {
                viewModel.query = searchText
                viewModel.offset = 0
                viewModel.getPlaces()
            } else {
                viewModel.query = ""
                viewModel.clearPlacesData()
                Toast.makeText(requireContext(), getString(R.string.error_search_text), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun initMap() {
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun observePlaces() {
        viewModel.placesLiveData.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.LOADING -> updateLoadingView(true)
                Resource.Status.SUCCESS -> {
                    updateLoadingView(false)
                    viewModel.processPlacesResponse(it.data)
                    addMarker()
                }
                Resource.Status.ERROR -> updateLoadingView(false)
            }
        })
    }

    private fun updateLoadingView(isLoading: Boolean){
        binding.apply {
            pbLoading.isVisible = isLoading
            icSearch.isVisible = !isLoading
        }
    }

    private fun addMarker() {
        if (mMap != null) {
            if(viewModel.offset == 0) removeAllMarker()

            for (i in viewModel.placesList.indices) {
                val coordinates = viewModel.placesList[i]?.coordinates

                coordinates?.apply {
                    if(latitude != null && longitude != null)  {
                        val location = LatLng(latitude.toDouble(), longitude.toDouble())
                        mMap?.apply {
                            val marker =  addMarker(MarkerOptions().position(location))
                            moveCamera(CameraUpdateFactory.newLatLng(location))
                            animateCamera(CameraUpdateFactory.zoomTo(10F))
                            viewModel.addMarker(marker, viewModel.placesList[i]?.id)
                        }
                    }
                }
            }
        }
    }

    private fun removeAllMarker(){
        for (i in viewModel.markerList.indices) {
            val marker = viewModel.markerList[i].marker
            marker.remove()
        }
        viewModel.clearMarkerData()
    }

    override fun onMapReady(map: GoogleMap?) {
        if (map == null) return
        mMap = map
    }

}