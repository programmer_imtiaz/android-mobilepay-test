package com.imtiaz.mobilepaytest.ui

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.Marker
import com.imtiaz.mobilepaytest.data.entity.MapMarker
import com.imtiaz.mobilepaytest.data.remote.response.PlacesItem
import com.imtiaz.mobilepaytest.data.remote.response.PlacesResponse
import com.imtiaz.mobilepaytest.data.repository.AppRepository
import com.imtiaz.taskmanager.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(val repository: AppRepository): ViewModel() {

    val placesLiveData: MutableLiveData<Resource<PlacesResponse>> by lazy { MutableLiveData<Resource<PlacesResponse>>() }
    val placesList: MutableList<PlacesItem?> by lazy { mutableListOf() }
    val markerList: MutableList<MapMarker> by lazy { mutableListOf() }
    var offset = 0
    var query: String = ""

    private var totalPlaces = 0

    fun getPlaces(){
        if(query == "") return

        viewModelScope.launch {
            repository.getPlaces(query, placesLiveData)
        }
    }

    fun processPlacesResponse(response: PlacesResponse?) {
        response?.apply {
            totalPlaces = count ?: 0

            if(offset == 0){
                placesList.apply {
                    clear()
                    addAll(places ?: mutableListOf())
                    Log.e("PlacesCount", placesList.count().toString())
                    return
                }
            }
        }
    }

    fun clearPlacesData() {
        offset = 0
        totalPlaces = 0
        placesList.clear()
    }

    fun addMarker(marker: Marker?, id: String?) {
        if(marker == null || id == null) return

        markerList.add(MapMarker(marker, id))
    }

    fun clearMarkerData() = markerList.clear()
}