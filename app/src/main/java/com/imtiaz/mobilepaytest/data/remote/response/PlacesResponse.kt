package com.imtiaz.mobilepaytest.data.remote.response

import com.google.gson.annotations.SerializedName

data class PlacesResponse(

	@field:SerializedName("places")
	val places: List<PlacesItem?>? = null,

	@field:SerializedName("offset")
	val offset: Int? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("count")
	val count: Int? = null
)

data class Coordinates(

	@field:SerializedName("latitude")
	val latitude: String? = null,

	@field:SerializedName("longitude")
	val longitude: String? = null
)

data class PlacesItem(

	@field:SerializedName("area")
	val area: Area? = null,

	@field:SerializedName("score")
	val score: Int? = null,

	@field:SerializedName("life-span")
	val lifeSpan: LifeSpan? = null,

	@field:SerializedName("type-id")
	val typeId: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("disambiguation")
	val disambiguation: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("coordinates")
	val coordinates: Coordinates? = null
)

data class Area(

	@field:SerializedName("life-span")
	val lifeSpan: LifeSpan? = null,

	@field:SerializedName("type-id")
	val typeId: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("sort-name")
	val sortName: String? = null
)

data class LifeSpan(

	@field:SerializedName("ended")
	val ended: Any? = null,

	@field:SerializedName("end")
	val end: String? = null,

	@field:SerializedName("begin")
	val begin: String? = null
)
