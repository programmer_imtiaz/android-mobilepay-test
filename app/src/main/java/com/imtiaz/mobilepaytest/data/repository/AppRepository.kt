package com.imtiaz.mobilepaytest.data.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.imtiaz.mobilepaytest.data.remote.ApiService
import com.imtiaz.mobilepaytest.data.remote.response.PlacesResponse
import com.imtiaz.taskmanager.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class AppRepository @Inject constructor(val apiService: ApiService) {

    suspend fun getPlaces(query: String, liveData: MutableLiveData<Resource<PlacesResponse>>){
        liveData.postValue(Resource.loading())

        val value = withContext(Dispatchers.IO) {
            try {
                val response = apiService.getPlaces(query = query)
                if (response != null && response.isSuccessful) response
                else null
            }
            catch (exp: Exception){
                Log.e("Exception", exp.toString())
                null
            }
        }
        if(value != null) liveData.postValue(Resource.success(value.body()))
        else liveData.postValue(Resource.error())
    }
}