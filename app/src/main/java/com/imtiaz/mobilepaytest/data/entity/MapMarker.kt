package com.imtiaz.mobilepaytest.data.entity

import com.google.android.gms.maps.model.Marker

data class MapMarker(
    val marker: Marker,
    val id: String
)
