package com.imtiaz.mobilepaytest.data.remote

import com.imtiaz.mobilepaytest.data.remote.response.PlacesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiService {

    @GET("place/")
    suspend fun getPlaces(
        @Header("User-Agent") userAgent: String = "MobilePayTest/1.0.0 ( programmer.imtiaz@gmail.com )",
        @Query("query") query: String,
        @Query("fmt") fmt: String = "json"
    ) : Response<PlacesResponse>
}