package com.imtiaz.mobilepaytest.utils

const val CONNECT_TIMEOUT: Long = 30000
const val READ_TIMEOUT: Long = 30000
const val WRITE_TIMEOUT: Long = 30000